class Configuration(object):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:123@localhost/flaskblog_db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # +pymysql   is for mysql 8.0
